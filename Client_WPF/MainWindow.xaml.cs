﻿using System;
using System.Text;
using System.Windows.Controls;
using System.Net;
using System.Net.Sockets;
using System.Windows;

namespace Client_WPF
{
    // Main class
    public partial class MainWindow
    {
        private IPAddress IpAdress = IPAddress.Loopback;
        private readonly Socket ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private byte[] ClientBuffer = new byte[BUFFER_SIZE];
        private const int PORT = 1025;
        private const int BUFFER_SIZE = 1024;
        private bool recievedUsername = false;

        public MainWindow()
        {
            InitializeComponent();
            this.Title = "Client";
            ConnectToServer();
            LoopResponse();
        }

        // Connect to the server
        private void ConnectToServer()
        {
            AddChatRow(string.Format("Connecting to server {0} on {1}...", IpAdress, PORT));

            // Try connecting
            int attempts = 0;
            while (!ClientSocket.Connected)
            {
                try
                {
                    attempts++;
                    AddChatRow("Attempt: " + attempts);
                    ClientSocket.Connect(IpAdress, PORT);
                }
                catch (SocketException)
                {
                    //Console.Clear();
                }
            }

            AddChatRow("Connected to server!");
        }

        // Let user type data
        private void LoopResponse()
        {
            AddChatRow("Type \"/c exit\" to log out\n");

            // Bind a callback to recieving data from server
            ClientSocket.BeginReceive(ClientBuffer, 0, BUFFER_SIZE, SocketFlags.None, ReadResponse, ClientSocket);
        }

        // Read server response
        private void ReadResponse(IAsyncResult asyncResult)
        {
            int recieved = ClientSocket.EndReceive(asyncResult);
            if (recieved > 0)
            {
                byte[] data = new byte[recieved];
                Array.Copy(ClientBuffer, data, recieved);
                string text = Encoding.ASCII.GetString(data);

                if (!recievedUsername)
                {
                    // Setup username
                    Application.Current.Dispatcher.Invoke(() => AddChatRow("Your username is: " + text));
                    Application.Current.Dispatcher.Invoke(() => this.Title = "Client " + text);
                    recievedUsername = true;
                }
                else
                {
                    // Show response
                    if (text != "empty")
                        Application.Current.Dispatcher.Invoke(() => AddChatRow(text));
                }
            }

            ClientSocket.BeginReceive(ClientBuffer, 0, BUFFER_SIZE, SocketFlags.None, ReadResponse, ClientSocket);
        }

        // Visualise chat row
        private void AddChatRow(string text)
        {
            TextBlock chatRow = new TextBlock();
            chatRow.Text = text;
            stackpanel_chat.Children.Add(chatRow);
            scrollview_chat.ScrollToEnd();
            textbox_message.Text = "";
        }

        // Send message to server
        private void button_send_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Create data to send to server
            string message = textbox_message.Text;

            // Send data to server
            byte[] send_buffer = Encoding.ASCII.GetBytes(message);
            ClientSocket.Send(send_buffer, 0, send_buffer.Length, SocketFlags.None);

            // Exit if 'exit' command is send to server
            if (message.ToLower() == "/c exit")
            {
                //ClientSocket.Shutdown(SocketShutdown.Both);
                //ClientSocket.Close();
                button_send.IsEnabled = false;
            }
        }
    }
}
