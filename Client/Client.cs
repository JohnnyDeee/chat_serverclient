﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Client
    {
        private static IPAddress IpAdress = IPAddress.Loopback;
        private static readonly Socket ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private const int PORT = 1025;
        private const int BUFFER_SIZE = 1024;

        static void Main(string[] args)
        {
            Console.Title = "Client";
            ConnectToServer();
            LoopRequest();
        }

        // Connect to the server
        private static void ConnectToServer()
        {
            Console.WriteLine(string.Format("Connecting to server {0} on {1}...", IpAdress, PORT));

            // Try connecting
            int attempts = 0;
            while (!ClientSocket.Connected)
            {
                try
                {
                    attempts++;
                    Console.WriteLine("Attempt: " + attempts);
                    ClientSocket.Connect(IpAdress, PORT);
                }
                catch (SocketException)
                {
                    Console.Clear();
                }
            }

            Console.Clear();
            Console.WriteLine("Connected to server!");
        }

        // Let user type data
        private static void LoopRequest()
        {
            Console.WriteLine("Type \"/c exit\" to log out\n");

            // Get first message from server
            // it contains our username
            ReadResponse(true);

            while (true)
            {
                // Create data to send to server
                Console.Write("Message: ");
                string message = Console.ReadLine();

                // Send data to server
                byte[] send_buffer = Encoding.ASCII.GetBytes(message);
                ClientSocket.Send(send_buffer, 0, send_buffer.Length, SocketFlags.None);

                // Handle server response
                ReadResponse();

                // Exit if 'exit' command is send to server
                if (message.ToLower() == "/c exit")
                {
                    ClientSocket.Shutdown(SocketShutdown.Both);
                    ClientSocket.Close();
                    Console.WriteLine("Press a key to exit...");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }
        }

        // Read server response
        private static void ReadResponse(bool initial = false)
        {
            byte[] recieved_buffer = new byte[BUFFER_SIZE];
            int recieved = ClientSocket.Receive(recieved_buffer, SocketFlags.None);
            if (recieved > 0)
            {
                byte[] data = new byte[recieved];
                Array.Copy(recieved_buffer, data, recieved);
                string text = Encoding.ASCII.GetString(data);
                if (initial)
                {
                    // Setup username
                    Console.WriteLine("Your username is: " + text);
                    Console.Title = "Client " + text;
                }
                else
                {
                    // Show response
                    if (text != "empty")
                        Console.WriteLine(text);
                }
            }
        }
    }
}
