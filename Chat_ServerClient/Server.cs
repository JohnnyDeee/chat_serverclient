﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    // Class to use with the 'state' params
    public class StateObject
    {
        public Socket workSocket = null;
        public static readonly int BUFFER_SIZE = 1024;
        public byte[] buffer = new byte[BUFFER_SIZE];
    }

    // Class for connected sockets
    public class Client
    {
        public readonly string username;
        public Socket socket = null;

        // Constructor
        public Client(List<string> usedUsernames, Socket socket)
        {
            this.username = RandomString(usedUsernames);
            this.socket = socket;
        }

        // Generate random string
        private static string RandomString(List<string> usedUsernames)
        {
            string randomString = "";

            while (true)
            {
                Guid g = Guid.NewGuid();
                string guidString = Convert.ToBase64String(g.ToByteArray());
                guidString = guidString.Replace("=", "").Replace("+", "");
                string shortenGuid = guidString.Substring(0, 5);

                if (!usedUsernames.Contains(shortenGuid))
                {
                    randomString = shortenGuid;
                    break;
                }
            }

            return randomString;
        }
    }

    // Main class
    class Program
    {
        private const int PORT = 1025;
        private static EndPoint endPoint = new IPEndPoint(IPAddress.Any, PORT);
        private static Socket serverSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        private static List<Client> connectedClients = new List<Client>();
        private static List<string> usedUsernames = new List<string>();

        static void Main(string[] args)
        {
            Console.Title = "Server";
            Console.WriteLine("Starting server...");
            Initialize();
            Console.ReadKey();
        }

        // Start listening
        private static void Initialize()
        {
            serverSocket.Bind(endPoint);
            serverSocket.Listen(0);
            Console.WriteLine("Server started!\n");
            StateObject stateObj = new StateObject();
            serverSocket.BeginAccept(AcceptCallback, stateObj);
            Console.WriteLine("Waiting for connections...");
        }

        // Callback for recieving client connections
        private static void AcceptCallback(IAsyncResult asyncResult)
        {
            Socket socket = serverSocket.EndAccept(asyncResult);
            Client client = new Client(usedUsernames, socket);
            connectedClients.Add(client);
            Console.WriteLine("Client connected: " + client.username);
            socket.Send(Encoding.ASCII.GetBytes(client.username));
            byte[] buffer = new byte[1024];
            StateObject stateObj = new StateObject();
            stateObj.workSocket = socket;
            socket.BeginReceive(stateObj.buffer, 0, StateObject.BUFFER_SIZE, SocketFlags.None, RecieveCallback, stateObj);
            serverSocket.BeginAccept(AcceptCallback, asyncResult.AsyncState);
        }

        // Callback for recieving client data
        private static void RecieveCallback(IAsyncResult asyncResult)
        {
            StateObject stateObj = (StateObject)asyncResult.AsyncState;
            Socket socket = stateObj.workSocket;
            int recieved = socket.EndReceive(asyncResult);
            bool exit = false;

            if (recieved > 0)
            {
                // Get client data
                string text = Encoding.ASCII.GetString(stateObj.buffer, 0, recieved);
                Client client = connectedClients.Find(x => x.socket == socket);
                Console.WriteLine(string.Format("Recieved from client ({0}): {1}", client.username, text));

                // Setup response to send back to client
                string response = "empty";

                // Handle commands
                if (text.StartsWith("/c "))
                {
                    string command = text.Replace("/c ", "");
                    switch (command)
                    {
                        case "get_time":
                            response = DateTime.Now.ToLongTimeString();
                            break;
                        case "exit":
                            response = "You are now disconnected from the server.";
                            exit = true;
                            break;
                        default:
                            response = "This command is not recognized!";
                            break;
                    }

                    // Encode response text
                    byte[] data = Encoding.ASCII.GetBytes(response);

                    // Send response to client
                    socket.Send(data);
                    if (response != "empty")
                        Console.WriteLine(string.Format("Send to client ({0}): {1}", connectedClients.Find(x => x.socket == socket).username, response));
                }
                else // Handle normal messages
                {
                    response = string.Format("{0}: {1}", client.username, text);
                    byte[] data = Encoding.ASCII.GetBytes(response);

                    foreach (Client c in connectedClients)
                    {
                        //if (c.username != client.username)
                        //{
                        //    Socket sock = c.socket;
                        //    c.socket.Send(data);
                        //}
                        Socket sock = c.socket;
                        c.socket.Send(data);
                    }
                }
            }

            // Start recieving data again
            if (!exit)
                socket.BeginReceive(stateObj.buffer, 0, StateObject.BUFFER_SIZE, SocketFlags.None, RecieveCallback, stateObj);
            else if (exit) // Disconnect client from server
            {
                connectedClients.Remove(connectedClients.Find(x => x.socket == socket));
                Console.WriteLine(string.Format("Client ({0}) disconnected.", socket.LocalEndPoint.ToString()));
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
        }
    }
}
